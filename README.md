# edinburgh
edinburgh provides Python interfaces to the ascii data types used by Edinburgh Instruments products. Currently this package is limited to spectral data from an 
FS980. 

## Installation

A setup script (requires setuptools) is provided:

    python setup.py install

The current external dependencies are:
* photon_correlation
* spectrum_analysis

These links are broken at present. Contact the author if you are interested in using this code.

## Testing

The setup script can be used to run tests:

    python setup.py test

## Usage

### FS980
The FS980 spectrometer supports both spectra and lifetimes. A typical ascii-output file contains: 

1. An experiment title 
1. Metadata (dwell time, excitation settings, etc.)
1. Data (spectrum or lifetime)
Each file may include one or more measurements, one per column.

The interface F980Experiment automatically detects the measurement type and loads all data into memory for simple access:

    import edinburgh
    
    experiment = edinburgh.FS980Experiment("data.txt")
    print(experiment.title)
    for measurment in experiment:
      print(measurement.dwell_time)
