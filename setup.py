from setuptools import setup

setup(
    name="edinburgh",
    version="0.1",
    description="Interface to Edinburgh ascii data types",
    author="Thomas Bischof",
    author_email="tsbischof@gmail.com",
    url="https://github.com/tsbischof/edinburgh",
    license="MIT",
    
    packages=["edinburgh"],
    test_suite="tests",
    package_data={"tests": ["data/*.txt"]},
    
##    install_requires=["photon_correlation >= 0.1",
##                      "spectrum_analysis >= 0.1"]
    )
