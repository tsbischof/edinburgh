import os
import unittest

from pkg_resources import resource_filename

import edinburgh

class TestExcitationEmission(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.data = edinburgh.FLS980Experiment(
            resource_filename(__name__, "data/FLS980_excitation_emission.txt"))
        
    def test_title(self):
        self.assertEqual(self.data.title, "EmMap6")

    def test_labels(self):
        for measurement, wavelength in zip(self.data, range(300, 405, 5)):
            self.assertEqual(measurement.label,
                             "Ex1 {0:.2f}nm".format(wavelength))

    def test_dwell_times(self):
        for measurement in self.data:
            self.assertEqual(measurement.dwell_time, 0.1)

if __name__ == "__main__":
    unittest.main()
