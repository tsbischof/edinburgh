import csv
import itertools

from .Measurement import WavelengthMeasurement, LifetimeMeasurement

def _get_measurement(metadata_keys, metadata_values,
                     data_positions, data_counts):
    """An Edinburgh experiment is either a wavelength-resolved spectrum or a
time-resolved lifetime. This distinction is specified in the metadata for a
given measurement.

A measurement is specified from the following four lists:
1. Metadata keys: the first column of the ascii export header
2. Metadata values: the header column corresponding to the given measurement
3. Data positions: the first column of the data section
4. Data counts: the data column corresponding to the given measurement.

This method produces an object which subclasses either a Spectrum or Lifetime,
depending on the data type."""
    metadata = {key: value for (key, value) in zip(metadata_keys,
                                                   metadata_values)}

    kind = metadata["Type"]
    
    if kind in ["Emission Scan", "Excitation Scan"]:
        data_type = WavelengthMeasurement
    else:
        raise(TypeError("Measurement type not recognized: {}".format(kind)))

    valid_positions = list()
    valid_counts = list()

    for position, counts in zip(data_positions, data_counts):
        if not counts.strip():
            continue

        valid_positions.append(position)
        valid_counts.append(float(counts))
    
    return(data_type(valid_positions, valid_counts, metadata))

class FLS980Experiment:
    """Interface to ascii data exported by the FLS980. This is a comma-delmited
file containing three main sections:
1. An experiment title
2. Measurement metadata
3. Measurement data

This class acts as an interface to the data by providing methods to access a
particular measurement, which in turn contains either a spectrum or a lifetime
as its data."""
    def __init__(self, filename):
        with open(filename) as stream_in:
            reader = csv.reader(stream_in)
            self.title = next(reader)[0]

            header = list()
            data = list()

            # title is follows by a blank line
            next(reader)

            # now the metadata, which is terminated by a blank line
            for line in reader:
                if not line:
                    break

                header.append(line)

            # the remainder of the file is the data itself
            for line in reader:
                data.append(line)

        header = itertools.zip_longest(*header)
        data = itertools.zip_longest(*data)

        metadata_keys = next(header)
        positions = list(map(float, next(data)))

        self._measurements = list()

        for metadata, counts in itertools.zip_longest(header, data):
            # The export code adds a trailing delimiter, so we should ignore the
            # final column of the data.
            if not any(metadata):
                continue
            
            self._measurements.append(
                _get_measurement(metadata_keys, metadata,
                                 positions, counts))
    
    def __iter__(self):
        return(iter(self._measurements))

    def __getitem__(self, index):
        return(self._measurements[index])

    def __len__(self):
        return(len(self._measurements))
