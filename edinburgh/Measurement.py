from spectrum_analysis import Spectrum
from photon_correlation import Lifetime

class WavelengthMeasurement(Spectrum):
    def __init__(self, wavelengths, counts, metadata):
        self.wavelengths = wavelengths
        self.counts = counts
        self.metadata = metadata

    @property
    def label(self):
        return(self.metadata["Labels"])

    @property
    def dwell_time(self):
        return(float(self.metadata["Dwell Time"]))

class LifetimeMeasurement(Lifetime):
    pass
